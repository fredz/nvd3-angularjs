var app = angular.module('myApp', ['nvd3','ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.controller('myCtrl', function($scope) {
	
	
	
  $scope.colWidthContainerLeft = "hidden";
  $scope.colWidthContainerCenter = "col-md-12";
  
  $scope.activeDaily = 'active';
  $scope.activeWeekly = '';
  $scope.activeMontly = '';
  
  $scope.isNavCollapsed = true;
  //$scope.isCollapsed = false;
  //$scope.isCollapsedHorizontal = false;

  
  $scope.displayMenu = function(){
	  
	  if($scope.colWidthContainerLeft == "hidden"){
		  $scope.colWidthContainerLeft = "col-md-3";
		  $scope.colWidthContainerCenter = "col-md-9";		  			  
	  }else{
		  $scope.colWidthContainerLeft = "hidden";
		  $scope.colWidthContainerCenter = "col-md-12";		  			  
	  }
  }	
  
  $scope.isCollapsed = function(){
	//isNavCollapsed = !isNavCollapsed;	  
	console.log(!$scope.isNavCollapsed);
	$scope.isNavCollapsed = !$scope.isNavCollapsed;
  }
  
  $scope.dreamOption = function(dreamOption){
	  //console.log($scope.options.dispatch());
	  console.log($scope.data[0]);
	  $scope.data[0].disabled = true;
	  $scope.data[1].disabled = true;
	  $scope.data[2].disabled = true;
	  $scope.data[3].disabled = true;
	  
	switch (dreamOption) {
		case "spo2":
			$scope.data[0].disabled = false;
			break;
		case "sleepHour":
			$scope.data[1].disabled = false;
			break;
		case "pulse":
			$scope.data[2].disabled = false;
			break;			
		case "sleepPosition":
			$scope.data[3].disabled = false;
			break;			
		default:
			$scope.data[0].disabled = false;
	}	  
	  
	  
  }
  
  $scope.timeOption = function(timeOption){
	  
	$scope.activeDaily = '';
	$scope.activeWeekly = '';
	$scope.activeMontly = '';
	  
	switch (timeOption) {
		case "daily":
			$scope.data = dailyData();
			$scope.activeDaily = 'active';
			break;
		case "weekly":
			$scope.data = weeklyData();
			$scope.activeWeekly = 'active';
			break;
		case "monthly":
			$scope.data = monthlyData();
			$scope.activeMontly = 'active';
			break;			
		default:
			$scope.data = dailyData();			
	}
	
  }  
	
  $scope.options = {
            chart: {
                type: 'lineChart',
                height: 450,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 40,
                    left: 55
                },
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                useInteractiveGuideline: true,
                dispatch: {
                    stateChange: function(e){ 
						console.log($scope.data);
						console.log("stateChange"); 
					},
                    changeState: function(e){ console.log("changeState"); },
                    tooltipShow: function(e){ console.log("tooltipShow"); },
                    tooltipHide: function(e){ console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Daily'
                },
                yAxis: {
                    //axisLabel: 'Voltage (v)',
					axisLabel: 'Records',
                    tickFormat: function(d){
                        return d3.format('.02f')(d);
                    },
                    axisLabelDistance: -10
                },
                callback: function(chart){
                    console.log("!!! lineChart callback !!!");
                }
            },
            title: {
                enable: true,
                text: 'Daily Line Chart'
            },
            subtitle: {
                enable: true,
                //text: 'Subtitle for simple line chart. Lorem ipsum dolor sit amet, at eam blandit sadipscing, vim adhuc sanctus disputando ex, cu usu affert alienum urbanitas.',
				text: '',
                css: {
                    'text-align': 'center',
                    'margin': '10px 13px 0px 7px'
                }
            },
            caption: {
                enable: true,
                //html: '<b>Figure 1.</b> Lorem ipsum dolor sit amet, at eam blandit sadipscing, <span style="text-decoration: underline;">vim adhuc sanctus disputando ex</span>, cu usu affert alienum urbanitas. <i>Cum in purto erat, mea ne nominavi persecuti reformidans.</i> Docendi blandit abhorreant ea has, minim tantas alterum pro eu. <span style="color: darkred;">Exerci graeci ad vix, elit tacimates ea duo</span>. Id mel eruditi fuisset. Stet vidit patrioque in pro, eum ex veri verterem abhorreant, id unum oportere intellegam nec<sup>[1, <a href="https://github.com/krispo/angular-nvd3" target="_blank">2</a>, 3]</sup>.',
				html: '<b>Figure 1.</b> Daily Line Chart',
                css: {
                    'text-align': 'justify',
                    'margin': '10px 13px 0px 7px'
                }
            }
        };

        $scope.data = dailyData();

        /*Random daily Data Generator */
        function dailyData() {
            var spo2Values = [],sleepHourValues = [],
                pulseValues = [], faceValues = [];

            //Data is represented as an array of {x,y} pairs.
            for (var i = 0; i < 100; i++) {
                spo2Values.push({x: i, y: Math.sin(i/10)});
                sleepHourValues.push({x: i, y: i % 10 == 5 ? null : Math.sin(i/10) *0.25 + 0.5});
                pulseValues.push({x: i, y: .5 * Math.cos(i/10+ 2) + Math.random() / 10});
				faceValues.push({x: i, y: .5 * Math.cos(i/10)});
            }

            //Line chart data should be sent as an array of series objects.
            return [
                {
                    values: spo2Values,      //values - represents the array of {x,y} data points
                    key: 'SpO2', //key  - the name of the series.
                    color: '#ff7f0e',  //color - optional: choose your own line color.
                    strokeWidth: 2,
                    classed: 'dashed'
                },
                {
                    values: sleepHourValues,
                    key: 'Sleep Hours',
                    color: '#2ca02c'
                },
                {
                    values: pulseValues,
                    key: 'Pulse',
                    color: '#7777ff',
                    //area: true      //area - set to true if you want this line to turn into a filled area chart.
                },
                {
                    values: faceValues,
                    key: 'Face Orientation',
                    color: 'yellow',                    
                }
            ];
        };
		
        /*Random weekly Data Generator */
        function weeklyData() {
            var spo2Values = [],sleepHourValues = [],
                pulseValues = [], faceValues = [];

            //Data is represented as an array of {x,y} pairs.
            for (var i = 0; i < 100; i++) {
                spo2Values.push({x: i, y: Math.sin(i*0.75/10)});
                sleepHourValues.push({x: i, y: i % 10 == 2 ? null : Math.sin(i/10) *0.25 + 0.75});
                pulseValues.push({x: i, y: .5 * Math.cos(i/10+ 2) + Math.random()*0.02 / 10});
				faceValues.push({x: i, y: .5 * Math.cos(i/10+4)});
            }

            //Line chart data should be sent as an array of series objects.
            return [
                {
                    values: spo2Values,      //values - represents the array of {x,y} data points
                    key: 'SpO2', //key  - the name of the series.
                    color: '#ff7f0e',  //color - optional: choose your own line color.
                    strokeWidth: 2,
                    classed: 'dashed'
                },
                {
                    values: sleepHourValues,
                    key: 'Sleep Hours',
                    color: '#2ca02c'
                },
                {
                    values: pulseValues,
                    key: 'Pulse',
                    color: '#7777ff',
                    //area: true      //area - set to true if you want this line to turn into a filled area chart.
                },
                {
                    values: faceValues,
                    key: 'Face Orientation',
                    color: 'yellow',                    
                }
            ];
        };	

        /*Random monthly Data Generator */
        function monthlyData() {
            var spo2Values = [],sleepHourValues = [],
                pulseValues = [], faceValues = [];

            //Data is represented as an array of {x,y} pairs.
            for (var i = 0; i < 100; i++) {
                spo2Values.push({x: i, y: Math.cos(i*1.5/10)});
                sleepHourValues.push({x: i, y: i % 10 == 5 ? null : Math.cos(i*0.7/10) *0.75 + 0.25});
                pulseValues.push({x: i, y: .5 * Math.sin(i/10+ 1) + Math.random()*0.4 / 10});
				faceValues.push({x: i, y: .58 * Math.sin(i*0.4/10)});
            }

            //Line chart data should be sent as an array of series objects.
            return [
                {
                    values: spo2Values,      //values - represents the array of {x,y} data points
                    key: 'SpO2', //key  - the name of the series.
                    color: '#ff7f0e',  //color - optional: choose your own line color.
                    strokeWidth: 2,
                    classed: 'dashed'
                },
                {
                    values: sleepHourValues,
                    key: 'Sleep Hours',
                    color: '#2ca02c'
                },
                {
                    values: pulseValues,
                    key: 'Pulse',
                    color: '#7777ff',
                    //area: true      //area - set to true if you want this line to turn into a filled area chart.
                },
                {
                    values: faceValues,
                    key: 'Face Orientation',
                    color: 'yellow',                    
                }
            ];
        };			
		
});


